# Printing Outputs

Let's create a new project named `outputs`, and open it in our editor. Substitute `code` for the editor that you've chosen.

```sh
cargo new outputs
code outputs
```

## Main Function

The entry point for a Rust application is the `main()` function in the `main.rs` file: it is the function that will be called when your executable starts.

Open the file at `src/main.rs`, and delete the `main()` function's content.

```rust
fn main() {

}
```

## Output Streams

Starting out, it's important to learn about the three standard streams, "standard input", "standard output" and "standard error".

Every program essentially starts with two mouths: one explicitly for communicating errors, warnings, and other annoying information generally destined for the user (standard error, a.k.a. stderr); and another for communicating its result (standard output, a.k.a. stdout), either to the console, to a file, or to another program.

The standard input (a.k.a stdin) is responsible for bringing the data to the program (for example what you type on the console).

```rust
fn main() {
    println!("I am speaking to {}", "standard output"); // the comma separated values given after the first argument go in the `{}` slots
    eprintln!("I am speaking to {}", "standard error");
}
```

In the example above, `println!()` will print the given text to standard output, and `eprintln!()` will print its given text to standard error. You can compile this with `cargo build`, and run it in a terminal with `target/debug/outputs`.

If you want to experiment with ignoring streams, try these out:

```sh
# /dev/null is an endless pit: what gets there disappears
# > is the sh symbol to change the destination of stdout/stderr
target/debug/outputs 2>/dev/null # 2 is stderr, so only listen to stdout
target/debug/outputs 1>/dev/null # 1 is stdout,
target/debug/outputs &>/dev/null # & is both stderr and stdout, so get nothing
```

## Bonus: Under the Hood (Not for the faint hearted)

So how do those macro work? File descriptors, and the `io::Write` trait. It's okay to not fully understand this example right now. This is a sneak peek into some low level details that are hidden behind our macro abstractions.

> File descriptors are numbers that represent open files in a program. File descriptor 0 is standard input, file descriptor 1 is standard output, and file descriptor 2 is standard error.

> Traits are behaviors shared among types. For example a Walk trait could be implemented on the Dog and Cat types. With this, you could for example make a `walk_toward` function that doesn't care about the type it receive, as long as it walks. More on this later.


```rust
use std::io::{self, Write};

fn main() {
    {
        // Lock access to the stdout file descriptor.
        let stdout = io::stdout();
        let mut stdout_locked = stdout.lock();

        // Write directly to it with the Write trait.
        stdout_locked.write(b"I am speaking to ");
        stdout_locked.write(b"standard output");
        stdout_locked.write(b"\n");

        // Drop the lock.
    }

    {
        // Do the same with stderr's file descriptor.
        let stderr = io::stderr();
        let mut stderr_locked = stderr.lock();

        stderr_locked.write(b"I am speaking to ");
        stderr_locked.write(b"standard error");
        stderr_locked.write(b"\n");

        // Drop the lock
    }
}
```
