# Functions

Within functions, you will define statements, each ending with a `;`. Statements are defined serially, and are executed in the order that they are listed. The next statement is only executed once the previous has been completed.

A statement without an `;` is treated as an expression, which generate and return values for assignment.

Each function declares that it receives and returns a set of optional input and output arguments. Functions must declare the types of the inputs and outputs, and the compiler enforces that the programmer honors these restrictions. If the function returns a value, then the value of the last expression (a statement without the ending `;`) is used as the return value.

```rust
fn name_of_function(parameter: InputType1, parameter: InputType2) -> OutputType {
    // First statement
    let first_value = other_function(parameter1);

    // Second statement
    let second_value = another_function(parameter2);

    // Final expression, whose output is the return value
    last_function(first_value, second_value)
}
```

- `fn` declares that a function is about to be defined.
- `name_of_function` is the name of this new function.
- `(parameter: InputType1, parameter: InputType2)` defines the input arguments for this function
- `-> OutputType` declares the return value, which is optional
- Within `(parameter: InputType1, parameter: InputType2)`
    - `parameter1` is the name of the first input argument
    - `InputType1` is the type that `parameter1` will be.

## Example

We'll start with some basic math for demonstrating functions. We supply an input argument (the side of a square), and get an output argument back (the calculated area). We then take the value we received, and use it as an input argument for our `println!()` macro.

```rust
// The function `area_of_square` takes a single number named input and returns another number
fn area_of_square(input: f32) -> f32 { // f32 is a type for decimal numbers (like 1.1)
    input * input
}

fn main() {
    // println outputs the value to the console
    println!("{}", area_of_square(5.0));
}
```
