# The Basics

This section will cover everything that you need to know to get started with writing applications in Rust. You can use this section as a reference to accomodate the Rust API documentation for when you are building your first applications. That means that we will push theory harder than practice in this section in order to cover all of the necessary concepts to make practice possible.
