# Cargo Package Manager

Cargo is the name of Rust's package manager and build system. You will use this create new projects, build them, run them, test them, and even document them. Cargo is backed by the Crates.io package repository, where you can both fetch and publish crates to be instantly shared across the Internet.

## Crates.io & Lib.rs

Looking for a library? Chances are, someone's already written it for you, you just need to find it. Try searching between [Crates.io](https://crates.io) and [Lib.rs](https://libs.rs). It doesn't exist? Now's the time to make your mark in history to be the first person to develop and publish it!

## Docs.rs

Looking for documentation for a crate on the Crates.io mirror? Check out [Docs.rs](docs.rs). Every crate published to the Crates.io mirror is automatically documented by this documentation service. That way, even if a maintainer did not take the time to describe the API, you don't need to scan their code to understand how to use it. That said, if you plan to make a library, **never** omit to write examples and clear instructions, it makes the lives of fellow developers so much easier.

## Hello World

Now, let's get started. Create a project from the terminal with the new subcommand:

```sh
cargo new hello
cd hello
```

This initializes a new directory with a "hello world" template. You can build it, and run it, like so:

```sh
cargo build
cargo run
```

All it will do at this point is merely output

```
Hello, world!
```

That's cool, but relatively useless. Alas, that's all we know how to do right now.

> **Note:**
>
> This builds a debug binary, which is unoptimized. Pass the `--release` flag to make it optimized.

## Maintaining the Cago.toml spec

If you take a look into the directory that was created, you'll notice that there are at least two files: the **src** directory which contains all of the **.rs** source files, and the **Cargo.toml** file which defines project metadata. Within the `Cargo.toml` file, it is possible to define project information, binaries and their entry source files, workspaces, and add dependencies. [See the Cargo book for details on the Cargo manifest](https://doc.rust-lang.org/cargo/).

## Difference between binaries and libraries

In the world of software development, there are two types of software: **applications**, and **libraries**. Applications, also known as **binaries**, have an entry point where the execution of the program begins, and ultimately ends. Libraries, which we refer to as **crates**, are the building blocks by which applications are built from.

With **cargo**, the default is to create a binary project, though it may also be given a flag to create a library with `cargo new --lib project-name`. The main difference between a library project and a binary project is that a library contains a **lib.rs** file, whereas a binary contains a **main.rs** file.

Technically, projects can contain both a library and a binary, too. The **main.rs** file can import the library contained at **lib.rs** so that a project may be used as either a library, or built into a standalone binary itself. Projects may also define multiple binaries in the **Cargo.toml** spec for the project.
