# Assignments

Values in software in one of two forms: constants and variables. Constants are values which are known to be true, and remain the same throughout the lifetime of the program. Variables are values which will be computed at runtime — there's a cost to their creation.

## Constants

One example of a value which might be a constant is a calculation of pi. The value of pi will never change, so we don't need to compute it. The below example prints pi to "standard output".

```rust
const PI: f32 = 3.14159265359;

fn area_of_circle(radius: f32) -> f32 {
    PI * radius.powf(2.0)
}

fn main() {
    println!("{}", area_of_circle(5.0));
}
```

> In the above, the `const` keyword denotes that a new constant, named `PI`, will be defined. The type of a constant must be known, and so we shall set that to `f32`. The `=` operator declares that the name defined on the left hand side will be assigned the value on the right hand side (exactly like with basic algebra, where one would write `pi = 3.1416`).

Running this should output:

```
78.53982
```

## Variables

Rust extends the concepts of variables by making them *immutable* by default. A variable which is immutable cannot be changed once it has been computed. Most languages are mutable by default, thereby allowing them to change. In contrast, in rust, you must explicitely annotate the variables as mutable.

```rust
fn main() {
    let immutable = 10;
    let mut mutable = 5;

    mutable = immutable + mutable; // works
    immutable = immutable + mutable; // won't compile

    println!("value: {}", mutable);
}
```

`let` is the keyword in Rust for creating a new variable. If the `mut` keyword does not follow that, the variable created will be immutable. Attempting to mutate an immutable variable is a compiler error. If you need to mutate a value, make sure to assign it with `let mut` instead of `mut`.

> **Note:**
>
> Compilers can produce faster code when variables are marked as immutable. The more assumptions that a compiler can make, the higher the chance that it will generate faster code. Compilers can assume that immutable variables will never change.
