# Introduction

> This is book currently under construction in its early stages. Expect gaps of knowledge and entire sections missing.

The target audience for this tutorial are those whose first experience with programming will be Rust. Despite being oriented towards beginners, my approach to teaching is based more on practice than theory.

You will learn to write software through example and practice. Although there will be some terms, and explanations of these terms, the majority of the focus will lie on teaching useful patterns that you can use in practice now. Theory can be taught after you've learned by practice.

> **NOTE:**
>
> Throughout the guide, we assume that you're developing software from a Linux desktop, such as [Pop!_OS](https://system76.com/pop). Not using Linux? It's a free, powerful platform for software development, written by programmers, for programmers, and the world. It makes our job of teaching you to program much easier.
