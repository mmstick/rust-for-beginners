# Familiarize Yourself With Git

When initializing a new project, Cargo will also initialize the git version control system for you.

A version control system is a tool to allow someone to easily keep track of the changes that were made, as well as making it easy to revert to a previous version. The most basic example is a folder where one puts 'file.txt', 'file.v2.txt', 'file.v3.txt', etc. With git, multiple developers can each work concurrently on their local machines __on the very same software__. It also provides cool addons like binary searching the introduction of a bug on a more advanced level.

 Git works with patches — a set of differences in your code. When you want to save a set of change, your create a *commit*: a patch with a description for further reference.

 You can see a list of files that have changed since the last commit was made with the `status` subcommand of `git`.

```sh
git status
```

Add all of these files to the patch, and then commit them.

```sh
git add .
git commit -m ':tada: Initialized first project

Description of the plan for the new project.' # use the -m option to indicate the message. If you omit it, an editor will open it
```

## Remotes
A remote is a place that is accessible over the internet to store your code as well as sharing it with other developers. Think Google Docs, but for code. Those are generally on GitLab, GitHub or BitBucket.

### Add Remote & Push Changes Upstream

If you have a repository on GitHub or GitLab, you can add the remote for it like so:

```sh
git remote add origin <URL> # add the remote source named origin pointing to the url
git push origin master
```

### Pulling Upstream Changes

If you need to pull the latest changes from the *upstream* (a.k.a. remote):

```sh
git pull origin master # pull the changes that are saved for the branch master on the origin remote
```

## Branch
A branch is a set of commits that share a same goal. For example, a developer could create the branch `headers` on an editor to work on the changes needed to add the headers to the software, while keeping the *master* branch - the branch that is traditionally user-facing - usable.

### Branch Creation

```sh
git checkout -b new-branch-name
# do thing
git push origin new-branch-name
```

### Branch Switching

And switch to another branch with

```sh
git checkout master
```

### Fetching Upstream Branches

Wanting to work on a branch from an other person's repo? Easy:

```sh
git fetch <URL or remote> # update your info on the repo
git checkout branch-on-repo
```

## Commit Amendments

If you ever need to make a few changes and add them to the last commit you made, you can run

```sh
git commit --amend
git push origin master --force
```

Try not to force push too often, if at all. It's okay if it's on a *branch*, but it gets annoying to other contributors if you do this directly to the `master` branch.
