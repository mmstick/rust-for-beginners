# Scopes

When variables are created, they are automatically assigned a finite lifetime which is defined by the scope in which they were created. This means that once the scope has been closed, any variable created on that scope will be dropped, and therefore cease to exist.

Scopes are defined in syntax by enclosing statements between the `{` and `}` characters. They may also be used as expressions, using the last value in the scope as the return value for that scope, as seen below.

```rust
fn main() {
    let foo = "foo";

    let z = {
        let x = 1;
        let y = 2;
        x + y
    };

    println!("foo: {}; z: {}", foo, z);
}
```

In the above example, the `x` and `y` variables will be dropped at the end of the scope which assigns to `z`, and thus they are inaccessible outside of the scope that they are defined.

## Pattern: Scoped Borrow

This is often useful when you need to borrow a variable's value for a short period of time for repeated reuse. The borrow will be dropped once its scope has been dropped, allowing you to borrow it elsewhere.

```rust
let mut variable = ...;

{
    // `variable` is now "locked" so that it can't be mutated.
    let immutable = &variable;

    function1(immutable);
    function2(immutable);
    // `immutable` is dropped here, so it is no longer "locked".
}

// variable can now be borrowed mutably
function3(&mut variable);
```
